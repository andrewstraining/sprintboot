package com.capgemini.training;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

@Controller
@EnableAutoConfiguration
@SpringBootApplication
public class SprintbootAPI {
	
	@Autowired
    Database database;
	
    @RequestMapping("/")
    @ResponseBody
    String home() throws SQLException {
    	return "Hello World!";
    }

    @RequestMapping("/test")
    @ResponseBody
    String test() throws SQLException {
        return "This is a test";
    }

    @RequestMapping(value = "/addvalue", method = RequestMethod.POST)
    @ResponseBody
    String testpost(@RequestBody String payload) {
        return payload;
    }


    @RequestMapping(value = "/getvalue/{id}", method = RequestMethod.GET)
    @ResponseBody
    String myvalue(@PathVariable String id) {
        return id;
    }
  
    public static void main(String[] args) throws Exception {
        SpringApplication.run(SprintbootAPI.class, args);
    }
   
}