package com.capgemini.training;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.stereotype.Component;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.ResultSetMetaData;
import com.mysql.jdbc.Statement;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;


@Component
public class Database {

	@Value("${db.username}")
	public String username;
	
	@Value("${db.password}")
	public String password;
	
	@Value("${db.host}")
	public String host;

	@Value("${db.database}")
	public String database;
	
    public String databaseQuery(String query) throws SQLException {
    	
		String result = null;
		
		try {
			
			MysqlDataSource dataSource = new MysqlDataSource();
			
			dataSource.setUser(username);
			dataSource.setPassword(password);
			dataSource.setServerName(host);
			dataSource.setDatabaseName(database);

			Connection conn = (Connection) dataSource.getConnection();
			Statement stmt = (Statement) conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			ResultSetMetaData rsmd = (ResultSetMetaData) rs.getMetaData();

			int columnsNumber = rsmd.getColumnCount();
			
			result = "";
			
			boolean first = true;
			
			while( rs.next() ) {
				
				if( ! first ) {
					result += ",";
				}
				
				for( int i = 1; i <= columnsNumber; i++ ) {
					result += ("\"" + rsmd.getColumnName(i) + "\":\"" + rs.getString(i) + "\"");
				}
				
				first = false;
			}
			
			rs.close();
			stmt.close();
			conn.close();
		}
		catch( SQLException e ) {
			System.out.println(e.getMessage());
			
			result = null;
		}
		
		return result;
    }
    
    public boolean databaseStatement(String statement) throws SQLException {
    	
		MysqlDataSource dataSource = new MysqlDataSource();
		
		dataSource.setUser(username);
		dataSource.setPassword(password);
		dataSource.setServerName(host);

		Connection conn = (Connection) dataSource.getConnection();
		Statement stmt = (Statement) conn.createStatement();
		
		boolean success = true;
		
		try {
			stmt.executeUpdate(statement);
		}
		catch(SQLException e) {
			success = false;
		}

		stmt.close();
		conn.close();
		
		return success;
    }
    
	//To resolve ${} in @Value
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertyConfig() {
		return new PropertySourcesPlaceholderConfigurer();
	}
}
