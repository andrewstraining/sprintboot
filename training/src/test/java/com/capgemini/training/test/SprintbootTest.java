package com.capgemini.training.test;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.capgemini.training.Database;
import com.capgemini.training.SprintbootAPI;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { SprintbootAPI.class })
@EnableConfigurationProperties
public class SprintbootTest {

	@Autowired
	Database database;
	
	@Test
	public void testQuery() throws SQLException {
		
		String result = database.databaseQuery("SELECT * FROM training.training");
		
		if( result == null ) {
			fail();
		}
		
		System.out.println("Result was: " + result);
	}

}
